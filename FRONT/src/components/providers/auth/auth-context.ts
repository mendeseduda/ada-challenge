import { createContext } from 'react';

type Auth = {
  isLoggedIn: boolean;
  token: string;
  login: (userName: string, password: string) => Promise<{ error?: string }>;
};

export const AuthContext = createContext<Auth>(null!);
