import '@uiw/react-md-editor/markdown-editor.css';
// eslint-disable-next-line import/no-extraneous-dependencies
import '@uiw/react-markdown-preview/markdown.css';
import React, { useEffect, useState } from 'react';
import { Box, Divider, Flex, IconButton, Input, Text } from '@chakra-ui/react';
import MDEditor from '@uiw/react-md-editor';
import { EditIcon } from '@chakra-ui/icons';
import { Card } from '@/types/card.type';
import { EditableControls } from './editable-controls';
import rehypeSanitize from 'rehype-sanitize';

type Props = {
  card: Card;
  onSaveChanges: (card: Card) => void;
  onDeleteCard: (card: Card) => void;
  onMoveToLeft?: () => void;
  onMoveToRight?: () => void;
};

export const EditableCard: React.FC<Props> = ({
  card,
  onSaveChanges,
  onDeleteCard,
  onMoveToLeft,
  onMoveToRight,
}) => {
  const [title, setTitle] = useState(card.title);
  const [content, setContent] = useState(card.content);
  const [inEdit, setInEdit] = useState(false);

  const isDirty = title.length > 0 && content.length > 0;

  useEffect(() => setInEdit(card.editing), [card, card.editing]);

  const saveChanges = () => {
    if (isDirty) {
      onSaveChanges({
        id: card.id,
        uuid: card.uuid,
        title,
        content,
        list: card.list,
        editing: false,
      });
    }
  };

  const cancelEditing = () => {
    if (!card.id) {
      onDeleteCard(card);
    }

    setInEdit(false);
    setTitle(card.title);
    setContent(card.content);
  };

  return (
    <Box p={2} shadow="md" borderWidth="1px">
      <Flex
        px={0}
        pb={2}
        borderBottomWidth={1}
        borderColor="gray.200"
        fontSize="xl"
        display="flex"
        flexDirection="row"
        alignItems="center"
      >
        {inEdit ? (
          <Input
            autoFocus
            value={title}
            onChange={(e) => setTitle(e.target.value)}
            placeholder="Insira um título"
          />
        ) : (
          <Text flex={1} textAlign="center">
            {title}
          </Text>
        )}
        {!inEdit && (
          <IconButton
            aria-label="edit"
            icon={<EditIcon boxSize={3} />}
            onClick={() => setInEdit(true)}
          />
        )}
      </Flex>
      <Divider borderColor="gray.200" />
      <MDEditor
        textareaProps={{ 'aria-label': 'md-textarea' }}
        data-color-mode="light"
        preview={inEdit ? 'edit' : 'preview'}
        hideToolbar
        height={200}
        value={content}
        previewOptions={{
          rehypePlugins: [[rehypeSanitize]],
        }}
        onChange={(value) => setContent(value || '')}
      />
      <EditableControls
        isEditing={inEdit}
        onCancelEditing={cancelEditing}
        onSaveChanges={() => saveChanges()}
        onDelete={() => onDeleteCard(card)}
        onMoveToLeft={onMoveToLeft}
        onMoveToRight={onMoveToRight}
      />
    </Box>
  );
};
