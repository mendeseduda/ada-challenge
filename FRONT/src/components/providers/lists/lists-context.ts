import { createContext } from 'react';

export const ListsContext = createContext<{
  lists: string[];
}>(null!);
