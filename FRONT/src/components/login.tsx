import { Flex, FormControl, FormLabel, Input, Button, Text } from '@chakra-ui/react';
import React, { useState } from 'react';
import { useAuth } from './providers/auth/use-auth';

const Login: React.FC = () => {
  const { login } = useAuth();
  const [userName, setUserName] = useState('');
  const [password, setPassword] = useState('');
  const [errorMessage, setErrorMessage] = useState<string | null>(null);

  const handleLogin = async () => {
    const { error } = await login(userName, password);

    if (error) {
      setErrorMessage(error);
    }
  };

  const onEnter = (key: string) => {
    if (key === 'Enter') {
      handleLogin();
    }
  };

  return (
    <Flex
      onKeyUp={(e) => onEnter(e.key)}
      w="100vw"
      h="100vh"
      justifyContent="center"
      alignItems="center"
    >
      <Flex flexDirection="column" p={5} borderRadius="6px" gap={3} shadow="md" borderWidth="1px">
        <FormControl isRequired>
          <FormLabel>Usuário</FormLabel>
          <Input placeholder="Usuário" onChange={(e) => setUserName(e.target.value)} />
        </FormControl>
        <FormControl isRequired>
          <FormLabel>Senha</FormLabel>
          <Input
            type="password"
            placeholder="Senha"
            onChange={(e) => setPassword(e.target.value)}
          />
        </FormControl>
        <Text color="red.500">{errorMessage}</Text>
        <Button onClick={handleLogin}>Login</Button>
      </Flex>
    </Flex>
  );
};

export default Login;
