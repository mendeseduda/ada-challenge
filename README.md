# Desafio Técnico - Frontend

## Rodando o projeto

### Para rodá-lo, faça:

```console
> docker compose up
```

### Para executar os testes do Frontend

```console
> docker compose run --rm -it frontend yarn test
```

Ele responderá na porta 3000.

## Observações

- Apesar do requisito dizer sobre todo novo card surgir da lista ToDo, optei por permitir um novo card em cada coluna
- Não me preocupei em criar um .env
- Não me preocupei em isolar as chamadas da API
- Preferi não testar dummy components
