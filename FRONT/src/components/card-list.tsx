import { Card } from '@/types/card.type';
import { AddIcon } from '@chakra-ui/icons';
import { Box, Divider, Heading, StackDivider, VStack, Button } from '@chakra-ui/react';
import React from 'react';
import { EditableCard } from './editable-card';
import { useCards } from './providers/cards/use-cards';
import { v4 as randomUUID } from 'uuid';
import { useLists } from './providers/lists/use-lists';
import { useAuth } from './providers/auth/use-auth';

type Props = {
  list: string;
};

export const CardList: React.FC<Props> = ({ list }) => {
  const lists = useLists();
  const { cards, setCards } = useCards();
  const { token } = useAuth();

  const createOrEditCard = async (card: Card) => {
    if (card.id) {
      const body = {
        id: card.id,
        titulo: card.title,
        conteudo: card.content,
        lista: card.list,
      };

      return fetch(`http://localhost:3001/cards/${card.id}`, {
        method: 'PUT',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`,
        },
      });
    }

    const body = {
      titulo: card.title,
      conteudo: card.content,
      lista: card.list,
    };

    return fetch('http://localhost:3001/cards', {
      method: 'POST',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
  };

  const saveCard = async (editedCard: Card) => {
    const response = await createOrEditCard(editedCard);
    const createdCard = await response.json();

    const editedCardIndex = cards.findIndex((card) => card.uuid === editedCard.uuid);
    const cardsCopy = cards.filter((_, index) => index !== editedCardIndex);

    cardsCopy.splice(editedCardIndex, 0, {
      id: createdCard.id,
      uuid: editedCard.uuid,
      title: createdCard.titulo,
      content: createdCard.conteudo,
      list: createdCard.lista,
      editing: false,
    });

    setCards([...cardsCopy]);
  };

  const removeCard = async (deletedCard: Card) => {
    if (!deletedCard.id) {
      const deletedCardIndex = cards.findIndex((card) => card.uuid === deletedCard.uuid);
      const cardsCopy = cards.filter((_, index) => index !== deletedCardIndex);

      setCards([...cardsCopy]);
      return;
    }

    const response = await fetch(`http://localhost:3001/cards/${deletedCard.id}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();

    const loadedCards = data.map(
      (card: any): Card => ({
        id: card.id,
        uuid: randomUUID(),
        title: card.titulo,
        content: card.conteudo,
        list: card.lista,
        editing: false,
      }),
    );
    setCards(loadedCards);
  };

  const moveCard = async (futureList: string, selectedCard: Card) => {
    const body = {
      id: selectedCard.id,
      titulo: selectedCard.title,
      conteudo: selectedCard.content,
      lista: futureList,
    };

    const response = await fetch(`http://localhost:3001/cards/${selectedCard.id}`, {
      method: 'PUT',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
      },
    });
    const editedCard = await response.json();
    const editedCardIndex = cards.findIndex((card) => card.uuid === selectedCard.uuid);
    const cardsCopy = cards.filter((_, index) => index !== editedCardIndex);

    cardsCopy.splice(editedCardIndex, 0, {
      id: editedCard.id,
      uuid: selectedCard.uuid,
      title: editedCard.titulo,
      content: editedCard.conteudo,
      list: editedCard.lista,
      editing: false,
    });

    setCards([...cardsCopy]);
  };

  const addNewCard = () => {
    const newCard: Card = { uuid: randomUUID(), title: '', content: '', list, editing: true };
    setCards([...cards, newCard]);
  };

  const getMoveCardFunctions = (card: Card) => {
    const listIndex = lists.indexOf(card.list);
    const fns: any = {};

    if (listIndex - 1 >= 0) {
      fns.moveToLeft = () => {
        const futureList = lists[listIndex - 1];
        moveCard(futureList, card);
      };
    }

    if (listIndex + 1 <= lists.length - 1) {
      fns.moveToRight = () => {
        const futureList = lists[listIndex + 1];
        moveCard(futureList, card);
      };
    }

    return fns;
  };

  const listCards = cards.filter((card) => card.list === list);

  return (
    <Box flex={1}>
      <Heading py={3} textAlign="center">
        {list}
      </Heading>
      <Divider borderColor="gray.200" />
      <VStack
        pt={4}
        px={7}
        spacing={4}
        align="stretch"
        divider={<StackDivider borderColor="gray.200" />}
      >
        {listCards.map((card) => {
          const { moveToLeft, moveToRight } = getMoveCardFunctions(card);

          return (
            <EditableCard
              key={card.uuid}
              card={card}
              onSaveChanges={saveCard}
              onDeleteCard={removeCard}
              onMoveToLeft={moveToLeft}
              onMoveToRight={moveToRight}
            ></EditableCard>
          );
        })}
        <Button leftIcon={<AddIcon></AddIcon>} onClick={addNewCard}>
          Adicionar cartão
        </Button>
      </VStack>
    </Box>
  );
};
