import React, { useEffect, useState } from 'react';
import { AuthContext } from './auth-context';

export const AuthProvider: React.FC<{ children: JSX.Element }> = ({ children }) => {
  const [token, setToken] = useState<string | null>(null);

  useEffect(() => {
    const storagedToken = sessionStorage.getItem('token');

    if (storagedToken) {
      setToken(storagedToken);
    }
  }, []);

  const login = async (userName: string, password: string): Promise<{ error?: string }> => {
    try {
      const body = { login: userName, senha: password };
      const response = await fetch('http://localhost:3001/login', {
        method: 'POST',
        body: JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.status === 401) {
        return { error: 'Email ou senha incorretos' };
      }

      const responseToken = await response.json();

      setToken(responseToken);
      sessionStorage.setItem('token', responseToken);
      return {};
    } catch {
      return { error: 'Email ou senha incorretos' };
    }
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn: !!token, token: token || '', login }}>
      {children}
    </AuthContext.Provider>
  );
};
