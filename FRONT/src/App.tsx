import React from 'react';
import { Providers } from './components/providers';

import './index.css';
import Home from './Home';

function App() {
  return (
    <>
      <Providers>
        <Home></Home>
      </Providers>
    </>
  );
}

export default App;
