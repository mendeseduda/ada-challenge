import { useContext } from 'react';
import { AuthContext } from './auth-context';

export const useAuth = () => {
  const context = useContext(AuthContext);

  return context;
};
