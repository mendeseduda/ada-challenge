import { Card } from '@/types/card.type';
import React from 'react';
import { useState } from 'react';
import { CardsContext } from './cards-context';

export const CardsProvider: React.FC<{ defaultCards?: Card[]; children: JSX.Element }> = ({
  defaultCards,
  children,
}) => {
  const [cards, setCards] = useState<{ data: Card[] }>({ data: defaultCards || [] });

  return <CardsContext.Provider value={{ cards, setCards }}>{children}</CardsContext.Provider>;
};
