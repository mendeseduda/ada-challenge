import React from 'react';
import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { AuthProvider } from '../../src/components/providers/auth/auth-provider';
import { CardsProvider } from '../../src/components/providers/cards/cards-provider';
import { ListsProvider } from '../../src/components/providers/lists/lists-provider';
import { CardList } from '../../src/components/card-list';
import { Card } from '../../src/types/card.type';

const mockFetch = (response: any) => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(response),
    }),
  ) as any;
};

const setup = (cards: Card[] = [], list: string = 'ToDo') => {
  return render(
    <AuthProvider>
      <CardsProvider defaultCards={cards}>
        <ListsProvider lists={['ToDo', 'Doing', 'Done']}>
          <CardList list={list}></CardList>
        </ListsProvider>
      </CardsProvider>
    </AuthProvider>,
  );
};

describe('card-list', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('renders card list', () => {
    const { baseElement } = setup();

    expect(baseElement).toBeInTheDocument();
  });

  it('adds a new empty card', async () => {
    setup();

    const addButton = screen.getByRole('button', { name: 'Adicionar cartão' });
    fireEvent.click(addButton);

    const titleInput = await screen.findByPlaceholderText('Insira um título');

    expect(titleInput).toBeInTheDocument();
  });

  it('creates a new card', async () => {
    mockFetch({ id: 'idc', titulo: 'test title', conteudo: '# content', lista: 'ToDo' });
    setup();
    const addButton = screen.getByRole('button', { name: 'Adicionar cartão' });
    fireEvent.click(addButton);

    const titleInput = await screen.findByPlaceholderText('Insira um título');
    const textArea = screen.getByLabelText('md-textarea');
    fireEvent.change(titleInput, { target: { value: 'test title' } });
    textArea.focus();
    fireEvent.change(textArea, { target: { value: '# content' } });
    textArea.blur();

    const confirmButton = screen.getByLabelText('check');
    fireEvent.click(confirmButton);

    expect(fetch).toBeCalledWith(
      expect.stringContaining('/cards'),
      expect.objectContaining({
        method: 'POST',
        body: JSON.stringify({
          titulo: 'test title',
          conteudo: '# content',
          lista: 'ToDo',
        }),
      }),
    );
  });

  it('updates card changes', async () => {
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'ToDo',
        editing: false,
      },
    ];
    mockFetch({
      id: 'idc1',
      titulo: 'test title updated',
      conteudo: '# content updated',
      lista: 'ToDo',
    });
    setup(cards);
    const editButton = screen.getByLabelText('edit');
    fireEvent.click(editButton);

    const titleInput = await screen.findByPlaceholderText('Insira um título');
    const textArea = screen.getByLabelText('md-textarea');
    fireEvent.change(titleInput, { target: { value: 'test title updated' } });
    textArea.focus();
    fireEvent.change(textArea, { target: { value: '# content updated' } });
    textArea.blur();

    const confirmButton = screen.getByLabelText('check');
    fireEvent.click(confirmButton);

    expect(fetch).toBeCalledWith(
      expect.stringContaining('/cards/idc1'),
      expect.objectContaining({
        method: 'PUT',
        body: JSON.stringify({
          id: 'idc1',
          titulo: 'test title updated',
          conteudo: '# content updated',
          lista: 'ToDo',
        }),
      }),
    );
  });

  it('deletes card', async () => {
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'ToDo',
        editing: false,
      },
    ];
    mockFetch([]);
    setup(cards);

    expect(screen.getByText('test title')).toBeInTheDocument();

    const deleteButton = screen.getByLabelText('delete');
    fireEvent.click(deleteButton);

    await waitFor(() => expect(screen.queryByText('test title')).not.toBeInTheDocument());

    expect(fetch).toBeCalledWith(
      expect.stringContaining('/cards/idc1'),
      expect.objectContaining({
        method: 'DELETE',
      }),
    );
  });

  it('disables move card left', () => {
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'ToDo',
        editing: false,
      },
    ];
    setup(cards);

    const moveLeftButton = screen.getByLabelText('left');

    expect(moveLeftButton).toBeDisabled();
  });

  it('disables move card right', () => {
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'Done',
        editing: false,
      },
    ];
    setup(cards, 'Done');

    const moveRightButton = screen.getByLabelText('right');

    expect(moveRightButton).toBeDisabled();
  });

  it('moves card left', async () => {
    mockFetch({
      id: 'idc1',
      titulo: 'test title',
      conteudo: '# content',
      lista: 'ToDo',
    });
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'Doing',
        editing: false,
      },
    ];
    setup(cards, 'Doing');

    const moveLeftButton = screen.getByLabelText('left');
    fireEvent.click(moveLeftButton);

    expect(fetch).toBeCalledWith(
      expect.stringContaining('/cards/idc1'),
      expect.objectContaining({
        method: 'PUT',
        body: JSON.stringify({
          id: 'idc1',
          titulo: 'test title',
          conteudo: '# content',
          lista: 'ToDo',
        }),
      }),
    );
  });

  it('moves card right', async () => {
    mockFetch({
      id: 'idc1',
      titulo: 'test title',
      conteudo: '# content',
      lista: 'Done',
    });
    const cards: Card[] = [
      {
        id: 'idc1',
        uuid: 'uuid',
        title: 'test title',
        content: '# content',
        list: 'Doing',
        editing: false,
      },
    ];
    setup(cards, 'Doing');

    const moveRightButton = screen.getByLabelText('right');
    fireEvent.click(moveRightButton);

    expect(fetch).toBeCalledWith(
      expect.stringContaining('/cards/idc1'),
      expect.objectContaining({
        method: 'PUT',
        body: JSON.stringify({
          id: 'idc1',
          titulo: 'test title',
          conteudo: '# content',
          lista: 'Done',
        }),
      }),
    );
  });
});
