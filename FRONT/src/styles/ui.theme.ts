import { extendTheme, ThemeConfig } from '@chakra-ui/react';

export const zIndexMap = {
  background: -1,
  ground: 0,
  hill: 1,
  sky: 2,
  stars: 3,
  hud: 4,
};

const config: ThemeConfig = {
  initialColorMode: 'light',
  useSystemColorMode: false,
};

const theme = extendTheme({
  config,
  fonts: {
    heading: 'Urbanist',
    body: 'Urbanist',
    text: 'Urbanist',
  },
});

export default theme;
