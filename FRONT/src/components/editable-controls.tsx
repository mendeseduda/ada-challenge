import React from 'react';
import {
  CheckIcon,
  CloseIcon,
  ChevronLeftIcon,
  DeleteIcon,
  ChevronRightIcon,
} from '@chakra-ui/icons';
import { ButtonGroup, IconButton } from '@chakra-ui/react';

type Props = {
  isEditing: boolean;
  onSaveChanges: () => void;
  onCancelEditing: () => void;
  onDelete: () => void;
  onMoveToLeft?: () => void;
  onMoveToRight?: () => void;
};

export const EditableControls: React.FC<Props> = ({
  isEditing,
  onSaveChanges,
  onCancelEditing,
  onDelete: onDeleteClick,
  onMoveToLeft,
  onMoveToRight,
}) => {
  const disableMoveToLeft = !onMoveToLeft;
  const disableMoveToRight = !onMoveToRight;

  if (isEditing) {
    return (
      <ButtonGroup justifyContent="end" size="sm" w="full" spacing={2} mt={2}>
        <IconButton aria-label="check" icon={<CheckIcon />} onClick={() => onSaveChanges()} />
        <IconButton
          aria-label="remove"
          icon={<CloseIcon boxSize={3} />}
          onClick={() => onCancelEditing()}
        />
      </ButtonGroup>
    );
  }

  return (
    <ButtonGroup justifyContent="space-between" size="sm" w="full" spacing={2} mt={2}>
      <IconButton
        isDisabled={disableMoveToLeft}
        aria-label="left"
        icon={<ChevronLeftIcon />}
        onClick={onMoveToLeft}
      />
      <IconButton aria-label="delete" icon={<DeleteIcon />} onClick={() => onDeleteClick()} />
      <IconButton
        isDisabled={disableMoveToRight}
        aria-label="right"
        icon={<ChevronRightIcon />}
        onClick={onMoveToRight}
      />
    </ButtonGroup>
  );
};
