import React from 'react';
import { ListsContext } from './lists-context';

export const ListsProvider: React.FC<{ lists: string[]; children: JSX.Element }> = ({
  lists,
  children,
}) => {
  return <ListsContext.Provider value={{ lists }}>{children}</ListsContext.Provider>;
};
