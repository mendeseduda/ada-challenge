import Login from '../components/login';
import { useAuth } from '../components/providers/auth/use-auth';
import React from 'react';

type Props = {
  children: React.ReactElement;
};

export const Layout: React.FC<Props> = ({ children }) => {
  const { isLoggedIn } = useAuth();

  if (isLoggedIn) {
    return <>{children}</>;
  }

  return <Login></Login>;
};
