import React, { useEffect } from 'react';
import { HStack, StackDivider } from '@chakra-ui/react';
import { Card } from './types/card.type';
import { useCards } from './components/providers/cards/use-cards';
import { CardList } from './components/card-list';
import { v4 as randomUUID } from 'uuid';
import { ListsProvider } from './components/providers/lists/lists-provider';
import { useAuth } from './components/providers/auth/use-auth';

const Home: React.FC = () => {
  const lists = ['ToDo', 'Doing', 'Done'];
  const { setCards } = useCards();
  const { token } = useAuth();

  const fetchCards = async () => {
    const response = await fetch('http://localhost:3001/cards', {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const data = await response.json();

    const loadedCards = data.map(
      (card: any): Card => ({
        id: card.id,
        uuid: randomUUID(),
        title: card.titulo,
        content: card.conteudo,
        list: card.lista,
        editing: false,
      }),
    );
    setCards(loadedCards);
  };

  const renderList = () => {
    return lists.map((list) => <CardList key={list} list={list}></CardList>);
  };

  useEffect(() => {
    fetchCards();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <ListsProvider lists={lists}>
      <HStack divider={<StackDivider borderColor="gray.200" />} alignItems="flex-start">
        {renderList()}
      </HStack>
    </ListsProvider>
  );
};

export default Home;
