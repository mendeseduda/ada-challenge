import React from 'react';
import { Layout } from '../../layout';
import theme from '../../styles/ui.theme';
import { ChakraProvider } from '@chakra-ui/react';
import { CardsProvider } from './cards/cards-provider';
import { AuthProvider } from './auth/auth-provider';

type Props = {
  children: React.ReactElement;
};

export const Providers: React.FC<Props> = ({ children }) => {
  return (
    <ChakraProvider theme={theme}>
      <AuthProvider>
        <CardsProvider>
          <Layout>{children}</Layout>
        </CardsProvider>
      </AuthProvider>
    </ChakraProvider>
  );
};
