export interface Card {
  id?: string;
  uuid: string;
  title: string;
  content: string;
  list: string;
  editing: boolean;
}
