import { Card } from '@/types/card.type';
import { useContext, useEffect } from 'react';
import { CardsContext } from './cards-context';

export const useCards = (defaultCards: Card[] | null = null) => {
  const { cards, setCards } = useContext(CardsContext);

  useEffect(() => {
    if (defaultCards !== null) {
      setCards({ data: defaultCards });
    }
  }, [defaultCards, setCards]);

  return {
    cards: cards.data,
    setCards: (newCards: Card[]) => {
      setCards({ data: newCards });
    },
  };
};
