import { Config } from '@jest/types';

const config: Config.InitialOptions = {
  setupFilesAfterEnv: ['./setupTests.ts'],
  roots: ['<rootDir>'],
  modulePaths: ['<rootDir>'],
  testEnvironment: 'jest-environment-jsdom',
  testMatch: ['./**/?(*.)+(spec|test).[tj]s?(x)'],
  preset: 'ts-jest',
  transformIgnorePatterns: ['<rootDir>node_modules/'],
  transform: {
    '^.+\\.(ts|tsx)?$': 'ts-jest',
    '^.+\\.(js|jsx)$': 'babel-jest',
  },
  moduleNameMapper: {
    '@/(.*)': '<rootDir>/src/$1',
    '\\.(s?css|less)': 'identity-obj-proxy',
  },
};

export default config;
