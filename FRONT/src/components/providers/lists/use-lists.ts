import { useContext } from 'react';
import { ListsContext } from './lists-context';

export const useLists = () => {
  const { lists } = useContext(ListsContext);

  return lists;
};
