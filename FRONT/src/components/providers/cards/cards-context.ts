import { Card } from '@/types/card.type';
import { Dispatch, createContext, SetStateAction } from 'react';

export const CardsContext = createContext<{
  cards: { data: Card[] };
  setCards: Dispatch<SetStateAction<{ data: Card[] }>>;
}>(null!);
