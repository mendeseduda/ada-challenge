import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';

import { AuthProvider } from '../src/components/providers/auth/auth-provider';
import { CardsProvider } from '../src/components/providers/cards/cards-provider';
import Home from '../src/Home';

const mockFetch = (response: any) => {
  global.fetch = jest.fn(() =>
    Promise.resolve({
      json: () => Promise.resolve(response),
    }),
  ) as any;
};

const setup = () => {
  return render(
    <AuthProvider>
      <CardsProvider>
        <Home></Home>
      </CardsProvider>
    </AuthProvider>,
  );
};

describe('Home', () => {
  it('renders home', () => {
    mockFetch([]);
    const { baseElement } = setup();

    expect(baseElement).toBeInTheDocument();
  });

  it('renders empty lists', () => {
    mockFetch([]);

    setup();

    const addCardButtons = screen.getAllByRole('button', { name: 'Adicionar cartão' });
    expect(addCardButtons).toHaveLength(3);
  });

  it('fetch cards from external service', async () => {
    mockFetch([
      {
        id: 'idc1',
        titulo: 'test title 1',
        conteudo: '# content',
        lista: 'ToDo',
      },
      {
        id: 'idc2',
        titulo: 'test title 2',
        conteudo: '# content',
        lista: 'ToDo',
      },
    ]);

    setup();

    await waitFor(() =>
      expect(fetch).toBeCalledWith(expect.stringContaining('/cards'), expect.any(Object)),
    );

    const cardsContent = await screen.findAllByText('content');

    expect(cardsContent).toHaveLength(2);
  });
});
